<?php get_header(); ?>

	<section class="hero cover">
		<div class="content">

			<div class="hero-image cover" style="background-image: url(<?php $image = get_field('careers_hero_image', 'options'); echo $image['url']; ?>);"></div>
			
			<div class="wrapper">
				<div class="info">
					<div class="headline">
						<h1>
							<?php if(get_field('careers_hero_headline', 'options')): ?>
								<?php the_field('careers_hero_headline', 'options'); ?>
							<?php else: ?>
								<?php the_title(); ?>
							<?php endif; ?>
						</h1>
					</div>

					<?php if(get_field('careers_hero_deck', 'options')): ?>
						<div class="copy p1">
							<?php the_field('careers_hero_deck', 'options'); ?>
						</div>
					<?php endif; ?>
				</div>
			</div>	
			
		</div>
	</section>

	<section id="why">
		<div class="wrapper">

			<div class="headline">
				<h2><?php the_field('careers_why_headline', 'options'); ?></h2>
			</div>

			<div class="copy p2">
				<?php the_field('careers_why_copy', 'options'); ?>
			</div>
			
		</div>
	</section>
	
	<section id="testimonials">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('careers_testimonials_headline', 'options'); ?></h2>
			</div>

			<div class="testimonials-wrapper">
				<?php if(have_rows('careers_testimonial','options')): while(have_rows('careers_testimonial', 'options')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'testimonial' ): ?>
						
						<div class="testimonial">

							<div class="copy p3">
								<?php the_sub_field('quote'); ?>
							</div>
				    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>


	<section id="careers-listings">
		<div class="wrapper">
			
			<div class="section-header headline">
				<h2>Careers Listings</h2>
			</div>

			<div class="careers-wrapper">

				<?php
					$args = array(
						'post_type' => 'careers',
						'posts_per_page' => 100
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="career">
						<div class="headline">
							<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>						
						</div>

						<div class="copy p3">
							<?php the_field('summary'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_permalink(); ?>">Learn More</a>
						</div>
					</div>

				<?php endwhile; endif; wp_reset_postdata(); ?>
				
			</div>



		</div>
	</section>


<?php get_footer(); ?>