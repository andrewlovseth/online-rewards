<?php

/*

	Template Name: Technology

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

	<section class="technology">
		<div class="wrapper">
			
			<?php if(have_rows('saas_technology')): while(have_rows('saas_technology')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'technology' ): ?>
					
					<div class="technology-service" data-aos="fade-left">
						<div class="photo">

							<?php if(get_sub_field('cta_link')): ?>
								<a href="<?php the_sub_field('cta_link'); ?>" rel="external">
									<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							<?php else: ?>
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>							
						</div>

						<div class="info">
							<div class="headline">
								<h2><?php the_sub_field('headline'); ?></h2>
							</div>

							<div class="copy p3">
								<?php the_sub_field('copy'); ?>
							</div>

							<?php if(get_sub_field('cta_link')): ?>
								<div class="cta">
									<a href="<?php the_sub_field('cta_link'); ?>"><?php the_sub_field('cta_label'); ?></a>
								</div>
							<?php endif; ?>	
						</div>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>