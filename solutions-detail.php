<?php

/*

	Template Name: Solutions Detail

*/

get_header(); ?>

	<section class="two-col">
		<div class="wrapper">
				
			<article>
				<div class="headline">
					<h4><?php echo get_the_title( $post->post_parent ); ?></h4>

					<?php if(get_field('headline')): ?>
						<h1 class="small"><?php the_field('headline'); ?></h1>
					<?php else: ?>
						<h1 class="small"><?php the_title(); ?></h1>
					<?php endif; ?>
				</div>


				<div class="featured-image mobile square">
					<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>


				<?php if(get_field('overview')): ?>
					<div class="overview">
						<div class="copy p2 extended">
							<?php the_field('overview'); ?>
						</div>
					</div>
				<?php endif; ?>

				<div class="features">					
					<?php if(have_rows('features')): while(have_rows('features')): the_row(); ?>
					 
					    <div class="feature">
					    	<div class="graphic">
					    		<a data-fancybox href="<?php $image = get_sub_field('graphic'); echo $image['url']; ?>">
					    			<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					    		</a>					    		
					    	</div>

					    	<div class="info">
					    		<div class="headline">
					    			<h2><?php the_sub_field('headline'); ?></h2>
					    		</div>

					    		<div class="copy p3">
					    			<?php the_sub_field('copy'); ?>
					    		</div>
					    	</div>					        
					    </div>

					<?php endwhile; endif; ?>
				</div>
			</article>

			<aside>
				<div class="featured-image square">
					<div class="photo">
						<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>					
				</div>

				<div class="explore">
					<div class="headline">
						<h3 class="small">Explore <?php echo get_the_title( $post->post_parent ); ?></h3>
					</div>

					<div class="links">
						<?php
							$current_page = $post->post_name;
							$parent_ID = $post->post_parent;
							$sub_pages = get_pages(
								array(
									'child_of' => $parent_ID,
									'sort_column' => 'menu_order',
									'sort_order' => 'asc',
								)
							);
							foreach( $sub_pages as $sub_page ): ?>

								<?php $slug = get_post_field( 'post_name', $sub_page );	?>

								<div class="link<?php if($current_page == $slug): ?> active<?php endif; ?>">
									<a href="<?php echo get_page_link( $sub_page->ID ); ?>"><?php echo $sub_page->post_title; ?></a>
								</div>

						<?php endforeach; ?>					
					</div>
				</div>

				<?php if(get_field('show_saas_solution')): ?>
					<div class="saas-solution">
						<div class="logo">
							<a href="<?php the_field('saas_solution_link'); ?>" rel="external">
								<img src="<?php $image = get_field('saas_solution_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="copy p3">
							<?php the_field('saas_solution_copy'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_field('saas_solution_link'); ?>" rel="external">Learn more</a>
						</div>						
					</div>
				<?php endif; ?>
			</aside>

		</div>
	</section>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>