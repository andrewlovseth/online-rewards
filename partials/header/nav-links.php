<div class="nav-links">

	<?php $links_ids = get_field('navigation', 'options', false, false); if( $links_ids ): ?>

		<?php foreach( $links_ids as $link_id ): ?>
			<a href="<?php echo get_the_permalink($link_id); ?>"><?php echo get_the_title($link_id); ?></a>
		<?php endforeach; ?>
		<a href="<?php echo site_url('/careers/'); ?>">Careers</a>

	<?php endif; ?>

</div>