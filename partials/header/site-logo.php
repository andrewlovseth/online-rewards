<div class="site-logo">
	<a href="<?php echo site_url('/'); ?>">
		<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" class="skip-lazy"  alt="<?php echo $image['alt']; ?>" />
	</a>
</div>