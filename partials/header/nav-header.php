<div class="nav-header">
	<div class="logo">
		<a href="<?php echo site_url('/'); ?>">
			<img src="<?php $image = get_field('logo_inverse', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>
	</div>

	<div class="close">
		<a href="#" class="close-btn"></a>
	</div>
</div>