<?php 
	if(is_page_template('solutions-category.php')) {
		$solutions = true;
		$slug = $post->post_name;
	} elseif(is_page_template('solutions-detail.php')) {
		$solutions = true;
		$post_data = get_post($post->post_parent);
		$slug = $post_data->post_name;
	} elseif(is_page_template('rewards.php')) {
		$solutions = true;
		$slug = $post->post_name;
	} else {
		$solutions = false;
	}
?>


<div class="solutions <?php if($solutions == true) { echo $slug . '-active'; } ?>">
	
	<?php if(have_rows('solutions_navigation', 'options')): while(have_rows('solutions_navigation', 'options')): the_row(); ?>

		<?php if(get_sub_field('icon')): ?>

		    <a href="<?php the_sub_field('link'); ?>" class="icon-btn <?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>-nav">
		    	<img class="icon normal skip-lazy" src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    	<img class="icon hover skip-lazy" src="<?php $image = get_sub_field('hover_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		        <?php the_sub_field('label'); ?>
		    </a>

		<?php else: ?>
	 
		    <a href="<?php the_sub_field('link'); ?>" class="<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>-nav">
		        <?php the_sub_field('label'); ?>
		    </a>

		<?php endif; ?>

	<?php endwhile; endif; ?>

</div>