<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-841244-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-841244-1');
</script>

<!-- Global site tag (gtag.js) - Google Ads: 1071997539 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-1071997539"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'AW-1071997539');
</script>


<!--What Converts-->
<script src="//scripts.iconnode.com/61958.js"></script>