<div class="newsletter">
	<h5>Newsletter</h5>

	<div class="copy p3 small">
		<p>Subscribe to receive news & updates</p>
	</div>

	<?php the_field('blog_form', 'options'); ?>
</div>