<div class="slide slide-programs">
	<div class="content">

		<div class="hero-image">
			<img src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>
			
		<div class="wrapper">
			<div class="info">

				<div class="headline">
					<h2><?php the_field('hero_headline'); ?></h2>
				</div>

				<div class="copy p1">
					<?php the_field('hero_deck'); ?>
				</div>

				<div class="ctas">
					<?php if(have_rows('hero_ctas')): while(have_rows('hero_ctas')): the_row(); ?>

						<div class="cta-icon">
						    <a href="<?php the_sub_field('link'); ?>">
						    	<span class="category"><?php the_sub_field('category'); ?></span>						        
						    	<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    	<span class="label"><?php the_sub_field('label'); ?></span>						        
						    </a>
						</div>
					 
					<?php endwhile; endif; ?>						
				</div>

			</div>
		</div>

	</div>			
</div>