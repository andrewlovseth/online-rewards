<section class="hero cover">
	<div class="content">

		<div class="hero-image cover" style="background-image: url(<?php $image = get_field('hero_image'); echo $image['url']; ?>);"></div>
		
		<div class="wrapper">
			<div class="info">
				<div class="headline">
					<h1><?php if(get_field('hero_headline')): ?><?php the_field('hero_headline'); ?><?php else: ?><?php the_title(); ?><?php endif; ?></h1>
				</div>

				<?php if(get_field('hero_deck')): ?>
					<div class="copy p1">
						<?php the_field('hero_deck'); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>	
		
	</div>
</section>