<section class="tour-cta">
	<div class="wrapper">

		<div class="tour-cta-wrapper" data-aos="fade-right">

			<div class="info">
				<div class="headline">
					<span><?php the_field('tour_cta_headline', 'options'); ?></span>
				</div>

				<div class="copy p2">
					<?php the_field('tour_cta_copy', 'options'); ?>
				</div>
			</div>

			<?php get_template_part('partials/header/tour-btn'); ?>

		</div>

	</div>
</section>