<section class="careers-cta">
	<div class="wrapper">

		<div class="careers-cta-wrapper" data-aos="fade-right">

			<div class="info">
				<div class="headline">
					<span><?php the_field('careers_cta_headline'); ?></span>
				</div>

				<div class="copy p2">
					<?php the_field('careers_cta_copy'); ?>
				</div>
			</div>

			<div class="careers-btn">
				<a href="<?php echo site_url('/careers/'); ?>"><?php the_field('careers_cta_label'); ?></a>
			</div>

		</div>

	</div>
</section>