<section class="tour-cta tour-form">
	<div class="wrapper">

		<div class="tour-cta-wrapper">

			<div class="info">
				<div class="headline">
					<span><?php the_field('tour_cta_headline', 'options'); ?></span>
				</div>

				<div class="copy p2">
					<?php the_field('tour_cta_copy', 'options'); ?>
				</div>
			</div>

			<div class="form-wrapper">
				<?php the_field('tour_cta_form', 'options'); ?>
			</div>

		</div>

	</div>
</section>