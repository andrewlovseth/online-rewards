<?php if(have_rows('faqs')): ?>

	<?php while(have_rows('faqs')) : the_row(); ?>	 
		<?php if( get_row_layout() == 'faq' ): ?>
			
			<section class="sc_fs_faq sc_card">
				<div>
					<h2><?php the_sub_field('question'); ?></h2>
					<div>
						<?php the_sub_field('answer'); ?>
					</div>
				</div>
			</section>

		<?php endif; ?>		 
	<?php endwhile; ?>

	<script type="application/ld+json">
	    {
	        "@context": "https://schema.org",
	        "@type": "FAQPage",
		    "mainEntity": [
			<?php
				$rowCount = count(get_field('faqs'));
				$i = 1;
				while(have_rows('faqs')) : the_row(); ?>
	        	{
	                "@type": "Question",
	                "name": "<?php the_sub_field('question'); ?>",
	                "acceptedAnswer": {
	                    "@type": "Answer",
	                    "text": "<?php $answer = get_sub_field('answer'); echo trim(strip_tags($answer)); ?>"
	                }
	            }<?php if($i < $rowCount): ?>,<?php endif; ?>
		     <?php $i++; endwhile; ?>
	        ]
	    }
	</script>

<?php endif; ?>