<?php $shortcode = get_field('faqs_shortcode'); if($shortcode): ?>

	<section class="faqs">
		<div class="wrapper">

			<?php if(get_field('faqs_headline')): ?>
				<div class="headline faqs-headline">
					<h2><?php the_field('faqs_headline'); ?></h2>
				</div>
			<?php endif; ?>
			
			<?php echo do_shortcode($shortcode); ?>

		</div>
	</section>

<?php endif; ?>