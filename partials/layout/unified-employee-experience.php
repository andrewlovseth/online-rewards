<section id="unified-employee-experience">
	<div class="wrapper">

		<div class="photo">
			<img src="<?php $image = get_field('about_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</div>

		
		<div class="infographic">
			<h2 class="infographic-header"><?php the_field('unified_employee_experience_headline'); ?></h2>

			<div class="infographic-wrapper">


				<?php if(have_rows('infographic')): while(have_rows('infographic')): the_row(); ?>
				 
				    <a href="#" class="spoke" data-aos="fade-up" data-copy="<h4><?php the_sub_field('title'); ?></h4><div class='copy p3'><?php the_sub_field('copy'); ?></div>">
				    	<div class="spoke-wrapper">

				    		<div class="icon-wrapper">
						    	<div class="icon">
						    		<img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						    	</div>

						    	<div class="title">
						    		<h4><?php the_sub_field('title'); ?></h4>
						    	</div> 				    			
				    		</div>				    		
				    	</div>
				    </a>

				<?php endwhile; endif; ?>
					
			</div>

			<div id="pop-up">
				<a href="#" class="close pop-up-close">✕</a>
				<div class="pop-up-overlay">

					
				</div>
			</div>		
		</div>

	</div>
</section>