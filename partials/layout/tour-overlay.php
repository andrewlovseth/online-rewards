<section id="tour-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

			<div class="info">
				<div class="close">
					<a href="#" class="tour-close-btn"></a>
				</div>

				<div class="scroll">
					<div class="headline">
						<h3><?php the_field('tour_overlay_headline', 'options'); ?></h3>
					</div>

					<div class="copy p4">
						<?php the_field('tour_overlay_copy'); ?>
					</div>

					<div class="tour-form">
						<?php the_field('tour_overlay_form', 'options'); ?>
					</div>
					
				</div>
			</div>
			
		</div>
	</div>
</section>