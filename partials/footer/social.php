<div class="footer-social">
	<div class="headline">
		<h5>Social</h5>
	</div>

	<div class="links">
		<?php if(have_rows('social_links', 'options')): while(have_rows('social_links', 'options')): the_row(); ?>
		 
		    <a href="<?php the_sub_field('link'); ?>" rel="external">
		        <img src="<?php $image = get_sub_field('icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		    </a>

		<?php endwhile; endif; ?>
	</div>



	<div class="technology-platform">
		<div class="headline">
			<h5>Technology Platform</h5>
		</div>

		<div class="logos">
			<?php $images = get_field('technology_platform_logos', 'options'); if( $images ): ?>
				<?php foreach( $images as $image ): ?>
					<div class="logo">
						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</div>