<div class="footer-utility">
	<div class="copyright">
		<p><?php the_field('copyright', 'options'); ?></p>
	</div>

	<div class="links">
		<?php if(have_rows('utility_links', 'options')): while(have_rows('utility_links', 'options')): the_row(); ?>
		 
		    <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>

		<?php endwhile; endif; ?>
	</div>
</div>