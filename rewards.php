<?php

/*

	Template Name: Rewards

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

	<section class="categories">
		<div class="wrapper">
			
			<?php if(have_rows('reward_categories')): while(have_rows('reward_categories')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'category' ): ?>
					
					<div class="category square" data-aos="fade-up">
						<div class="photo">
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />							
						</div>

						<div class="info">
							<div class="headline">
								<h3><?php the_sub_field('headline'); ?></h3>
							</div>

							<div class="copy p3">
								<?php the_sub_field('copy'); ?>
							</div>

							<div class="cta">
								<a href="<?php the_sub_field('cta_link'); ?>"><?php the_sub_field('cta_label'); ?></a>
							</div>
						</div>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<section class="global cover" style="background-image: url(<?php $image = get_field('global_graphic'); echo $image['url']; ?>);">
		<div class="wrapper">
			
			<div class="info">
				<div class="headline center" data-aos="fade-left">
					<h2><?php the_field('global_headline'); ?></h2>
				</div>

				<div class="copy p2" data-aos="fade-left">					
					<?php if(have_rows('global_items')): while(have_rows('global_items')): the_row(); ?>
					    <div class="item">
					        <p><?php the_sub_field('item'); ?></p>
					    </div>
					<?php endwhile; endif; ?>
				</div>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/layout/faqs'); ?>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>