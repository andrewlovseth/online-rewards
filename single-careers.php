<?php get_header(); ?>

	<section class="career-listing">
		<div class="wrapper">

			<div class="headline">
				<h1><?php the_title(); ?></h1>
			</div>

			<div class="copy p2 extended">
				<?php the_field('job_description'); ?>
			</div>
			
		</div>
	</section>
	
<?php get_footer(); ?>