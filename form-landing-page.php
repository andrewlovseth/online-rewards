<?php

/*

	Template Name: Form Landing Page

*/

get_header(); ?>

	<section class="form-landing-page cover" style="background-image:url(<?php $background_image = get_field('background_image'); echo $background_image['url'] ?>);">
		<div class="wrapper">

			<div class="info">
				<div class="headline">
					<div class="arc">
						<img src="<?php bloginfo('template_directory') ?>/images/blue-arc.png" alt="Blue Arc" />
					</div>
					<h1><?php the_field('headline'); ?></h1>
				</div>

				<div class="copy p2">
					<?php the_field('copy'); ?>
				</div>

				<?php if(get_field('graphic')): ?>
					<div class="graphic">
						<img src="<?php $image = get_field('graphic'); echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				<?php endif; ?>
			</div>

			<div class="form">
				<div class="form-wrapper">
					<?php the_field('form'); ?>
				</div>				
			</div>

		</div>
	</section>

<?php get_footer(); ?>