<?php

get_header(); ?>

	<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>

		<section class="generic article-wrapper">
			<div class="wrapper">
				
				<article>
					<section class="article-header">
						<div class="headline">
							<h1 class="editorial small"><?php the_title(); ?></h1>
						</div>
							
					</section>

					<section class="article-body">
						<div class="copy p2 extended">
							<?php the_content(); ?>
						</div>					
					</section>
				</article>

			</div>		
		</section>

	<?php endwhile; endif; ?>

	
<?php get_footer(); ?>