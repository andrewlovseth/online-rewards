<?php

/*

	Template Name: Solutions Category

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

	<section class="about">
		<div class="wrapper">

			<div class="headline">
				<h2><?php the_field('about_headline'); ?></h2>
			</div>	

			<div class="copy p1">
				<?php the_field('about_copy'); ?>
			</div>

			<?php if(get_field('about_cta')): ?>
				<div class="cta">
					<a href="<?php the_field('about_cta'); ?>"><?php the_field('about_cta_label'); ?></a>
				</div>
			<?php endif; ?>

			<div class="photo">
				<img src="<?php $image = get_field('about_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

		</div>
	</section>

	<section class="ideas">
		<div class="card-wrapper">
			<?php
				$current_page = $post->ID;
				$args = array(
					'post_type' => 'page',
					'post_parent' => $current_page,
					'posts_per_page' => 10,
					'orderby' => 'menu_order',
					'order' => 'ASC'
				);
				$query = new WP_Query( $args );
				if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<div class="card" data-page="<?php echo get_the_title(); ?>">
						<div class="photo">
							<a href="<?php echo get_permalink(); ?>">
								<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>
			    		
			    		<div class="info">
			    			<div class="headline">
			    				<h3><a href="<?php the_permalink(); ?>"><?php the_field('summary_headline'); ?></a></h3>
			    			</div>

			    			<div class="copy p3">
			    				<?php the_field('summary_copy'); ?>
			    			</div>

			    			<div class="cta">
			    				<a href="<?php the_permalink(); ?>">Learn More</a>
			    			</div>
			    		</div>
					</div>

			<?php endwhile; endif; wp_reset_postdata(); ?>

		</div>
	</section>

	<?php get_template_part('partials/layout/faqs'); ?>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>