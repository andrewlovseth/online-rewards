(function ($, window, document, undefined) {

	$(document).ready(function() {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});

		// Mobile Menu Toggle
		$('.menu-btn').click(function(){
			$(this).toggleClass('active');
			$('body').toggleClass('mobile-nav-open');

			return false;
		});

		// Menu Close Toggle
		$('.close-btn').click(function(){
			$('.menu-btn').toggleClass('active');
			$('body').toggleClass('mobile-nav-open');

			return false;
		});


		// Ideas Slider
		$('.ideas .card-wrapper').slick({
			dots: true,
			arrows: true,
			nextArrow: '<a href="#" class="next-arrow"></a>',
	        prevArrow: '<a href="#" class="prev-arrow"></a>',
		  	centerMode: true,
	  		slidesToShow: 1,
	  		variableWidth: true,
			autoplay: true,
			autoplaySpeed: 9000,
			customPaging: function(slider, i) {
				var title = $(slider.$slides[i]).data('page');
				return '<a>' + title + '</a>';
			},
		});


		// Clients Slider
		$('.clients-testimonials').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});


		// Clients Slider
		$('body.home section.hero').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});

		// Careers Slider
		$('.testimonials-wrapper').slick({
			arrows: false,
			dots: true,
	  		slidesToShow: 1,
			autoplay: true,
			autoplaySpeed: 6000,
		});


		$('.infographic .spoke .icon-wrapper').hover(function() {
			var infographic = $(this).closest('.infographic');
			var copy = $(this).closest('.spoke').data('copy');
			var popup = $('.infographic .pop-up-overlay');

			$(infographic).toggleClass('hover');
			$(popup).html(copy);
		});

		$('.spoke').click(function(){
			return false;
		});


		// Infographic Popup Hide
		$('.pop-up-close').click(function(){

			var infographic = $('.infographic');

			$(infographic).removeClass('hover');

			return false;
		});


		$('iframe[src*="youtube"]').parent().fitVids();

		// Animate on Scroll
		AOS.init({
			duration: 800,
			easing: 'ease-in-out-back',
			once: true,
			offset: 60,
		});



		$('[data-fancybox]').fancybox({
			infobar: false,
			buttons: [
				//"zoom",
				//"share",
				//"slideShow",
				//"fullScreen",
				//"download",
				//"thumbs",
				"close"
			],
			arrows: false,
			smallBtn: true,
		});
			
	});



	// Hide Overlay on ESC
	$(document).on('keydown', function(e) {
	    if ( e.keyCode === 27 ) {
			$('.menu-btn').removeClass('active');
			$('body').removeClass('mobile-nav-open');
	    }
	});


	if ( window.location.pathname == '/' || window.location.pathname == '/company/' ){

		var isInViewport = function (elem) {
		    var bounding = elem.getBoundingClientRect();
		    return (
		        bounding.top >= 0 &&
		        bounding.left >= 0 &&
		        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
		        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
		    );
		};

		var stats = document.querySelector('.stats');

		window.addEventListener('scroll', function (event) {
			if (isInViewport(stats)) {

				$('.counter').each(function() {
					var $this = $(this),
						countTo = $this.attr('data-count');

					$({ countNum: $this.text()}).animate({
							countNum: countTo
						}, {
							duration: 800,
							easing:'linear',
							step: function() {
							$this.text(Math.floor(this.countNum));
						},
							complete: function() {
							$this.text(this.countNum);
						}
					});    
				});
			}
		});

	}

})(jQuery, window, document);