<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section class="hero home-hero">

		<?php get_template_part('partials/home/programs-slide'); ?>

		<?php get_template_part('partials/home/rewards-slide'); ?>

	</section>


	<section id="employee-recognition">
		<div class="wrapper">

			<div class="headline">
				<div class="icon">
					<img src="<?php $image = get_field('employee_recognition_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				<h2 class="cat-headline"><?php the_field('employee_recognition_headline'); ?></h2>
				<h3 class="subheadline"><?php the_field('employee_recognition_sub_headline'); ?></h3>
			</div>

			<div class="photos" data-aos="fade-left">
				<?php $images = get_field('employee_recognition_gallery'); $count = 1; if( $images ): ?>
					<?php foreach( $images as $image ): ?>

						<div class="photo">
							<div class="content">
								
								<?php if($count == 3): ?>
									<div class="square square-tablet square-right">
										<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
									</div>
								<?php else: ?>
									<img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
								<?php endif; ?>
							
							</div>
						</div>

					<?php $count++; endforeach; ?>
				<?php endif; ?>
			</div>

			<div class="copy p2" data-aos="fade-left">
				<?php the_field('employee_recognition_copy'); ?>
			</div>

			<div class="cta center" data-aos="fade-left">
				<a href="<?php the_field('employee_recognition_cta'); ?>"><?php the_field('employee_recognition_cta_label'); ?></a>
			</div>

		</div>
	</section>


	<section id="b2b-incentives">
		<div class="wrapper">

			<div class="inset cover" style="background-image: url(<?php $image = get_field('b2b_incentives_image'); echo $image['url']; ?>);">
				<div class="content square">
					
					<div class="photo tablet-hide">
						<img src="<?php $image = get_field('b2b_incentives_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="info" data-aos="fade-up">
						<div class="headline">
							<div class="icon">
								<img src="<?php $image = get_field('b2b_incentives_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<h2 class="cat-headline"><?php the_field('b2b_incentives_headline'); ?></h2>
							<h3 class="subheadline"><?php the_field('b2b_incentives_sub_headline'); ?></h3>
						</div>

						<div class="copy p2">
							<?php the_field('b2b_incentives_copy'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_field('b2b_incentives_cta'); ?>"><?php the_field('b2b_incentives_cta_label'); ?></a>
						</div>
					</div>

				</div>
			</div>

		</div>
	</section>


	<section id="customer-loyalty">
		<div class="wrapper">
			
			<div class="photo" data-aos="fade-right">
				<div class="square square-right">
					<img src="<?php $image = get_field('customer_loyalty_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
			</div>

			<div class="info">
				<div class="headline">
					<div class="icon">
						<img src="<?php $image = get_field('customer_loyalty_icon'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<h2 class="cat-headline"><?php the_field('customer_loyalty_headline'); ?></h2>
					<h3 class="subheadline"><?php the_field('customer_loyalty_sub_headline'); ?></h3>
				</div>

				<div class="copy p2">
					<?php the_field('customer_loyalty_copy'); ?>
				</div>

				<div class="cta">
					<a href="<?php the_field('customer_loyalty_cta'); ?>"><?php the_field('customer_loyalty_cta_label'); ?></a>
				</div>
			</div>

		</div>
	</section>


	<section id="rewards" class="cover" style="background-image: url(<?php $image = get_field('rewards_image'); echo $image['url']; ?>);">
		<div class="content">
			<div class="wrapper">

				<div class="info">
					<div class="headline" data-aos="fade-up">
						<h1><?php the_field('rewards_headline'); ?></h1>
					</div>

					<div class="copy p2" data-aos="fade-up">
						<?php the_field('rewards_copy'); ?>
					</div>

					<div class="cta" data-aos="fade-up">
						<a href="<?php the_field('rewards_cta'); ?>"><?php the_field('rewards_cta_label'); ?></a>
					</div>
				</div>		

			</div>	
		</div>
	</section>


	<section id="about">
		<div class="wrapper">

			<div class="company square">
				<div class="photo">
					<img src="<?php $image = get_field('about_company_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<div class="info">
					<div class="info-wrapper"" data-aos="fade-left">
						<div class="logo">
							<img src="<?php $image = get_field('about_company_logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
						</div>
						
						<div class="copy p3">
							<?php the_field('about_company_copy'); ?>
						</div>

						<div class="cta">
							<a href="<?php the_field('about_cta'); ?>"><?php the_field('about_cta_label'); ?></a>
						</div>
					</div>
				</div>
			</div>

			<div class="stats">
				<div class="copy p2">
					<?php the_field('stats_copy'); ?>
				</div>	

				<div class="stats-wrapper">
					<?php if(have_rows('stats')): while(have_rows('stats')): the_row(); ?>
	 
					    <div class="stat">
					    	<h3 class="number">
					    		<span class="counter" data-count="<?php the_sub_field('ending_number'); ?>"><?php the_sub_field('starting_number'); ?></span><span class="unit"><?php the_sub_field('unit'); ?></span>
					    	</h3>
					    	
					    	<p><?php the_sub_field('description'); ?></p>				        
					    </div>

					<?php endwhile; endif; ?>					
				</div>
			</div>

			<div class="brands">
				<div class="headline" data-aos="fade-right">
					<h5><?php the_field('about_brands_headline'); ?></h5>
				</div>

				<div class="logos" data-aos="fade-left">
					<?php $images = get_field('about_brands_logos'); if( $images ): ?>
						<?php foreach( $images as $image ): ?>

							<div class="logo">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>			

						<?php endforeach; ?>
					<?php endif; ?>
				</div>
			</div>

		</div>
	</section>


	<section id="saas-technology">
		<div class="wrapper">
			
			<div class="headline" data-aos="fade-right">
				<h1><?php the_field('saas_technology_headline'); ?></h1>
			</div>

			<div class="copy p2" data-aos="fade-right">
				<?php the_field('saas_technology_copy'); ?>
			</div>

			<div class="logos" data-aos="fade-left">
				<?php $images = get_field('saas_technology_logos'); if( $images ): ?>
					<?php foreach( $images as $image ): ?>

						<div class="logo">
							<a href="<?php echo $image['title']; ?>">
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>			

					<?php endforeach; ?>
				<?php endif; ?>
			</div>

			<div class="cta" data-aos="fade-left">
				<a href="<?php the_field('saas_technology_cta'); ?>"><?php the_field('saas_technology_cta_label'); ?></a>
			</div>



		</div>
	</section>


	<section class="editorial">
		<div class="wrapper">

			<section class="section-header">
				<div class="headline" data-aos="fade-up">
					<h1 class="editorial">
						<a href="<?php $posts_page_id = get_option('page_for_posts'); echo get_permalink($posts_page_id ); ?>">
							<?php echo get_the_title($posts_page_id); ?>
						</a>
					</h1>
				</div>
			</section>

			<?php $featured_post = get_field('featured_post', $posts_page_id); if( $featured_post ): ?>

				<?php include( locate_template( 'partials/blog/featured-post.php', false, false ) ); ?>

			<?php endif; ?>

			<?php $curated_posts = get_field('curated_posts', $posts_page_id); if( $curated_posts ): ?>
				
				<?php include( locate_template( 'partials/blog/curated-posts.php', false, false ) ); ?>

			<?php endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>