<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

	<?php get_template_part('partials/header/header-scripts'); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

	<header>
		<div class="wrapper">

			<?php get_template_part('partials/header/menu'); ?>

			<?php get_template_part('partials/header/site-logo'); ?>

			<?php get_template_part('partials/header/solutions-nav'); ?>

			<?php get_template_part('partials/header/tour-btn'); ?>

		</div>
	</header>

	<nav>
		<div class="scroll">

			<?php get_template_part('partials/header/nav-header'); ?>

			<?php get_template_part('partials/header/nav-links'); ?>

			<?php get_template_part('partials/header/tour-btn'); ?>
			
		</div>
	</nav>

	<section id="main">