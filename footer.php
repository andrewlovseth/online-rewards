	</section>

	<footer>
		<div class="wrapper">

			<?php if(!is_front_page()): ?>
				<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
				    <?php
				    	if(function_exists('bcn_display')) {
				    		bcn_display();
				    	}
				    ?>
				</div>
			<?php endif; ?>

			<?php get_template_part('partials/footer/logo'); ?>

			<?php get_template_part('partials/footer/nav'); ?>

			<?php get_template_part('partials/footer/social'); ?>

			<?php get_template_part('partials/footer/utility'); ?>

		</div>
	</footer>

	<?php get_template_part('partials/layout/tour-overlay'); ?>

	<?php wp_footer(); ?>

</body>
</html>