<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

		<section class="offices">
			<div class="wrapper">				

				<?php if(have_rows('offices')): while(have_rows('offices')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'office' ): ?>
						
						<div class="office square" id="<?php echo sanitize_title_with_dashes(get_sub_field('city')); ?>">
							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="headline">
								<h2><?php the_sub_field('city'); ?></h2>
								<h3><?php the_sub_field('tagline'); ?></h3>								
							</div>

							<div class="copy">
								<h4>Address</h4>
								<p class="address"><?php the_sub_field('address'); ?></p>

								<h4>Phone</h4>
								<p class="phone"><?php the_sub_field('phone'); ?></p>

								<h4>Fax</h4>
								<p class="fax"><?php the_sub_field('fax'); ?></p>
							</div>				    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
				

			</div>
		</section>

	<?php get_template_part('partials/layout/careers-cta'); ?>

<?php get_footer(); ?>