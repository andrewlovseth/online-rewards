<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );
remove_theme_support('post-formats');



/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );


// Move Yoast to bottom
function yoasttobottom() {
    return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');



/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);



function bearsmith_remove_wp_block_library_css(){
 wp_dequeue_style( 'wp-block-library' );
 wp_dequeue_style( 'wp-block-library-theme' );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_remove_wp_block_library_css' );



// Register and noConflict jQuery 3.4.1
wp_register_script( 'jquery.3.4.1', 'https://code.jquery.com/jquery-3.4.1.min.js' );
wp_add_inline_script( 'jquery.3.4.1', 'var jQuery = $.noConflict(true);' );


// Enqueue custom styles and scripts
function bearsmith_enqueue_styles_and_scripts() {
    $uri = get_stylesheet_directory_uri();
    $dir = get_stylesheet_directory();

    $script_last_updated_at = filemtime($dir . '/js/site.js');
    $style_last_updated_at = filemtime($dir . '/style.css');

    // Add style.css and third-party css
    wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lato:400,400i,700,700i|Oswald:700|Arvo:700i' );
    wp_enqueue_style( 'fancybox', 'https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css' );
    wp_enqueue_style( 'aos', 'https://unpkg.com/aos@2.3.1/dist/aos.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css', '', $style_last_updated_at );


    // Add plugins.js & site.js (with jQuery dependency)
    wp_enqueue_script( 'custom-plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-site', get_stylesheet_directory_uri() . '/js/site.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
    wp_enqueue_script( 'custom-main', get_stylesheet_directory_uri() . '/js/main.js', array( 'jquery.3.4.1' ), $script_last_updated_at, true );
}
add_action( 'wp_enqueue_scripts', 'bearsmith_enqueue_styles_and_scripts' );








/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'date';
    $args['order'] = 'DESC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');