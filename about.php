<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

	<section id="mission">
		<div class="wrapper">

			<div class="headline">
				<h2><?php the_field('mission_headline'); ?></h2>
			</div>

			<div class="copy p2">
				<?php the_field('mission_copy'); ?>
			</div>

			<div class="principles">
				<div class="headline">
					<h3><?php the_field('principles_headline'); ?></h3>
				</div>

				<div class="copy">
					<?php if(have_rows('principles')): while(have_rows('principles')): the_row(); ?>
 
					    <div class="principle">
					        <p><?php the_sub_field('principle'); ?></p>
					    </div>

					<?php endwhile; endif; ?>
				</div>
			</div>
			
		</div>
	</section>


	<section id="clients">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('clients_headline'); ?></h2>
			</div>

			<div class="clients-testimonials">
				<?php if(have_rows('clients_testimonials')): while(have_rows('clients_testimonials')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'testimonial' ): ?>
						
						<div class="client">
							<div class="logo">
								<img src="<?php $image = get_sub_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="copy p3">
								<?php the_sub_field('copy'); ?>
							</div>
				    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>


	<section id="accolades">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('accolades_headline'); ?></h2>
			</div>

			<?php $home = get_page_by_path('home'); ?>

			<div class="stats">

				<div class="stats-wrapper">
					<?php if(have_rows('stats', $home)): while(have_rows('stats', $home)): the_row(); ?>
	 
					    <div class="stat">
					    	<h3 class="number">
					    		<span class="counter" data-count="<?php the_sub_field('ending_number'); ?>"><?php the_sub_field('starting_number'); ?></span><span class="unit"><?php the_sub_field('unit'); ?></span>
					    	</h3>
					    	
					    	<p><?php the_sub_field('description'); ?></p>				        
					    </div>

					<?php endwhile; endif; ?>					
				</div>
			</div>

			<div class="accolades-wrapper">
				<?php if(have_rows('accolades')): while(have_rows('accolades')): the_row(); ?>
				 
				    <div class="accolade copy p3">
				       <p> <?php the_sub_field('accolade'); ?></p>
				    </div>

				<?php endwhile; endif; ?>				
			</div>

		</div>
	</section>


	<section id="leadership">
		<div class="wrapper">
			
			<div class="headline">
				<h2><?php the_field('leadership_headline'); ?></h2>
			</div>

			<div class="leadership-wrapper">				
				<?php if(have_rows('leadership')): while(have_rows('leadership')) : the_row(); ?>
				 
				    <?php if( get_row_layout() == 'person' ): ?>
						
						<div class="person">
							<div class="photo">
								<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>

							<div class="info">
								<h4><?php the_sub_field('name'); ?></h4>
								<h5><?php the_sub_field('title'); ?></h5>
							</div>    		
						</div>
						
				    <?php endif; ?>
				 
				<?php endwhile; endif; ?>
			</div>

		</div>
	</section>

	<?php get_template_part('partials/layout/careers-cta'); ?>

<?php get_footer(); ?>