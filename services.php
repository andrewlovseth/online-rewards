<?php

/*

	Template Name: Services

*/

get_header(); ?>

	<?php get_template_part('partials/layout/hero'); ?>

	<section class="services">
		<div class="wrapper">
			
			<?php if(have_rows('services')): while(have_rows('services')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'service' ): ?>
					
					<div class="service" data-aos="fade-left">
						<div class="photo">
							<img src="<?php $image = get_sub_field('photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />							
						</div>

						<div class="info">
							<div class="headline">
								<h2><?php the_sub_field('headline'); ?></h2>
							</div>

							<div class="copy p3">
								<?php the_sub_field('copy'); ?>
							</div>
						</div>
					</div>
					
			    <?php endif; ?>
			 
			<?php endwhile; endif; ?>

		</div>
	</section>

	<?php get_template_part('partials/layout/tour-form'); ?>

<?php get_footer(); ?>